﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SuperPeopleApp;
using System.IO;
using System.Text;

namespace SuperPeopleTestApp
{
    [TestClass]
    public class UnitTest1
    {
        StringBuilder villain = new StringBuilder();
        StringBuilder superhero = new StringBuilder();

        [TestMethod]
        public void CheckIfFileExist()
        {
            Assert.IsTrue(File.Exists(SuperPowers.FILE_NAME));
        }

        [TestMethod]
        public void CheckIfFileIsReadable()
        {
            Assert.IsNotNull(File.OpenText(SuperPowers.FILE_NAME));
        }

        [TestMethod]
        public void CheckValueOfVillainsAndSuperheroes()
        {
            SuperPowers.ReadSuperPoweredFile(villain, superhero);

            Assert.IsNotNull(villain);
            Assert.IsNotNull(superhero);
        }

        [TestMethod]
        public void CheckIfAbleToWriteFile()
        {
            StringBuilder villain = new StringBuilder();
            StringBuilder superhero = new StringBuilder();

            SuperPowers.WriteToFile(villain, SuperPowers.VILLAIN_FILENAME);
            SuperPowers.WriteToFile(superhero, SuperPowers.SUPERHERO_FILENAME);

            Assert.IsTrue(File.Exists(SuperPowers.VILLAIN_FILENAME));
            Assert.IsTrue(File.Exists(SuperPowers.SUPERHERO_FILENAME));
        }
    }
}
