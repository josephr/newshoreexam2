﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml;

namespace WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        List<SuperPeople> GetSuperPeopleJson();

        [OperationContract]
        XmlElement GetSuperPeopleXml();
    }

    [DataContract]
    public class SuperPeople
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Type { get; set; }

    }
}
