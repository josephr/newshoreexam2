﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;

namespace WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string villainDetection = ConfigurationManager.AppSettings["VILLAIN_DETECTION"];
        public string fileName = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/" + ConfigurationManager.AppSettings["FILENAME"]);

        public List<SuperPeople> GetSuperPeopleJson()
        {
            List<SuperPeople> spList = new List<SuperPeople>();

            StringBuilder sb = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(sb))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("SuperPeople");
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = String.Empty;
                    while ((s = sr.ReadLine()) != null)
                    {
                        SuperPeople sp = new SuperPeople();
                        if (s.Contains(villainDetection))
                        {
                            sp.Name = s.Trim();
                            sp.Type = "Villain";
                        }
                        else
                        {
                            sp.Name = s.Trim();
                            sp.Type = "SuperHero";
                        }
                        spList.Add(sp);
                    }
                }
            }

            return spList;
        }

        public XmlElement GetSuperPeopleXml()
        {
            StringBuilder sb = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(sb))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("SuperPeople");
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = String.Empty;
                    while ((s = sr.ReadLine()) != null)
                    {
                        writer.WriteStartElement("Person");
                        if (s.Contains(villainDetection))
                        {
                            writer.WriteElementString("Name", s.Trim());
                            writer.WriteElementString("Type", "Villain");
                        }
                        else
                        {
                            writer.WriteElementString("Name", s.Trim());
                            writer.WriteElementString("Type", "SuperHero");
                        }
                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(sb.ToString());
            return xmlDocument.DocumentElement;
        }
    }
}
