﻿using System;
using System.IO;
using System.Text;

namespace SuperPeopleApp
{
    public class SuperPowers
    {
        //place all of this in web.config if its a wep application already
        public const string FILE_NAME = "RESGISTRADOS.DAT";
        private const string VILLAIN_DETECTION = "D";
        public const string VILLAIN_FILENAME = "Villains.dat";
        public const string SUPERHERO_FILENAME = "SuperHeroes.dat";

        static void Main(string[] args)
        {
            StringBuilder villain = new StringBuilder();
            StringBuilder superhero = new StringBuilder();

            //read the file and separate the villains and superheroes
            ReadSuperPoweredFile(villain, superhero);

            //time to write down the villains
            WriteToFile(villain, VILLAIN_FILENAME);

            //time to write down the superheroes
            WriteToFile(superhero, SUPERHERO_FILENAME);
        }


        /// <summary>
        /// 1. Check the file of the superpowered people
        /// 2. Read it line by line
        /// 3. Separate villains from superheroes based on character "D"
        /// </summary>
        /// <param name="villain"></param>
        /// <param name="superhero"></param>
        public static void ReadSuperPoweredFile(StringBuilder villain, StringBuilder superhero)
        {
            if (File.Exists(FILE_NAME))
            {
                using (StreamReader sr = File.OpenText(FILE_NAME))
                {
                    string s = String.Empty;
                    while ((s = sr.ReadLine()) != null)
                    {
                        if (s.Contains(VILLAIN_DETECTION))
                        {
                            villain.AppendLine(s);
                        }
                        else
                        {
                            superhero.AppendLine(s);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This will write the superheroes or villains in a file
        /// </summary>
        /// <param name="superPersons">Either a StringBuilder of villains or superheroes</param>
        /// <param name="filename">Name of the file for villains and superheroes</param>
        public static void WriteToFile(StringBuilder superPersons, string filename)
        {
            using (StreamWriter writer = new StreamWriter(filename))
            {
                writer.Write(superPersons);
            }
        }
    }
}
